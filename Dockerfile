FROM python:alpine

RUN apk update && apk upgrade && apk add musl-dev gcc sqlite-dev

RUN pip install --upgrade pip

RUN echo 'https://dl-cdn.alpinelinux.org/alpine/v3.9/main' >> /etc/apk/repositories

COPY ./requirements.txt /app/requirements.txt

WORKDIR /app
RUN pip install -r requirements.txt
COPY . /app

ENTRYPOINT [ "python" ]
CMD [ "run.py" ]
 
